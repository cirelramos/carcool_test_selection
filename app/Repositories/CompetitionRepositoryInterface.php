<?php

namespace App\Repositories;

interface CompetitionRepositoryInterface
{
    public function allCompetition();

    public function oneCompetition( $id );

}
