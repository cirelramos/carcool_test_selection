<?php

namespace App\Repositories;

use App\Models\Team;
use App\Traits\CallExternalApiTrait;
use Illuminate\Database\Eloquent\Collection;
use Spatie\QueryBuilder\QueryBuilder;

class TeamRepository implements TeamRepositoryInterface
{
    use CallExternalApiTrait;
    /**
     * @var Team
     */
    private $team;

    /**
     * TeamRepository constructor.
     * @param Team $team
     */
    public function __construct( Team $team )
    {
        $this->team = $team;
    }

    /**
     * @return Team[]|Collection
     */
    public function getAllTeams()
    {
        return $this->team->all();
    }

    public function getOneTeam( $id )
    {
        return $this->initModel()->where( 'id', $id )->with( 'squad' )->get();
    }

    /**
     * @return QueryBuilder
     */
    private function initModel()
    {
        return QueryBuilder::for( Team::class )->from( 'teams as t' );
    }

    /**
     * @param array $names
     * @return QueryBuilder
     */
    public function getTeamByNameQuery( array $names = [] )
    {
        return $this->initModel()->whereIn( 'name', $names );
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function insert( array $data )
    {
        return $this->team->insert( $data );
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getPlayersByTeam( $id )
    {
        $urlCustom = "{$this->init()}/{$id}";
        return $this->getData( $urlCustom, 'squad' );
    }

    /**
     * @return string
     */
    public function init()
    {
        return 'teams';
    }

}
