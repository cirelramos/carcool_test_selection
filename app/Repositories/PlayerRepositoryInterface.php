<?php

namespace App\Repositories;

interface PlayerRepositoryInterface
{

    /**
     * @param array $data
     * @return mixed
     */
    public function insert( array $data );

    /**
     * @param array $names
     * @return mixed
     */
    public function getPlayerByNameQuery( array $names = [] );

}
