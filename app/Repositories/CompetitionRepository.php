<?php

namespace App\Repositories;

use App\Traits\CallExternalApiTrait;

class CompetitionRepository implements CompetitionRepositoryInterface
{
    use CallExternalApiTrait;

    public function allCompetition()
    {
        return $this->getData( $this->init() );
    }

    public function init()
    {
        return 'competitions';
    }

    public function oneCompetition( $id )
    {
        $urlCustom = "{$this->init()}/{$id}";
        return $this->getData( $urlCustom, '', 'active' );
    }

    public function oneCompetitionWithTeams( $id )
    {
        $urlCustom = "{$this->init()}/{$id}/teams/";
        return $this->getData( $urlCustom, 'teams' );
    }

}
