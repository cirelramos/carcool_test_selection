<?php

namespace App\Repositories;

use App\Models\Player;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Spatie\QueryBuilder\QueryBuilder;

class PlayerRepository implements PlayerRepositoryInterface
{
    /**
     * @var Player
     */
    private $player;

    /**
     * PlayerRepository constructor.
     * @param Player $player
     */
    public function __construct( Player $player )
    {
        $this->player = $player;
    }

    public function getAllPlayers()
    {
        return $this->player->all( [ 'id', 'name', 'position', 'shirtNumber' ] );
    }

    /**
     * @param $id
     * @return Builder[]|Collection|\Illuminate\Support\Collection
     */
    public function getOnePlayer( $id )
    {
        return $this->initModel()->where( 'id', $id )->get( [ 'id', 'name', 'position', 'shirtNumber' ] );
    }

    /**
     * @return QueryBuilder
     */
    private function initModel()
    {
        return QueryBuilder::for( Player::class )->from( 'players as t' );
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function insert( array $data )
    {
        return $this->player->insert( $data );
    }

    /**
     * @param array $names
     * @return QueryBuilder
     */
    public function getPlayerByNameQuery( array $names = [] )
    {
        return $this->initModel()->whereIn( 'name', $names );
    }

}
