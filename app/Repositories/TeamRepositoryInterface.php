<?php

namespace App\Repositories;

interface TeamRepositoryInterface
{

    /**
     * @return mixed
     */
    public function getAllTeams();

    /**
     * @param $id
     * @return mixed
     */
    public function getOneTeam( $id );

    /**
     * @param array $names
     * @return mixed
     */
    public function getTeamByNameQuery( array $names = [] );

    /**
     * @param array $data
     * @return mixed
     */
    public function insert( array $data );

    /**
     * @param $id
     * @return mixed
     */
    public function getPlayersByTeam( $id );

}
