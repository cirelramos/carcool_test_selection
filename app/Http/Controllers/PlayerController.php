<?php

namespace App\Http\Controllers;

use App\Models\Player;
use App\Services\PlayerService;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;

class PlayerController extends Controller
{
    use ApiResponser;
    /**
     * @var PlayerService
     */
    private $playerService;

    /**
     * PlayerController constructor.
     * @param PlayerService $playerService
     */
    public function __construct( PlayerService $playerService )
    {
        $this->playerService = $playerService;
    }

    /**
     * @return mixed
     */
    public function index()
    {
        $data = $this->playerService->getAllPlayers();
        return $this->resultMessage( 'OK', 200, $data );
    }

    /**
     * @param Request $request
     * @param Player  $player
     * @return mixed
     */
    public function show( Request $request, Player $player )
    {
        $data = $this->playerService->getOnePlayer( $player->id );
        return $this->resultMessage( 'OK', 200, $data );
    }
}
