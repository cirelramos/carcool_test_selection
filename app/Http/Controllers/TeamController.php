<?php

namespace App\Http\Controllers;

use App\Models\Team;
use App\Services\TeamService;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;

class TeamController extends Controller
{
    use ApiResponser;
    /**
     * @var TeamService
     */
    private $teamService;

    /**
     * TeamController constructor.
     * @param TeamService $teamService
     */
    public function __construct( TeamService $teamService )
    {
        $this->teamService = $teamService;
    }

    /**
     * @return mixed
     */
    public function index()
    {
        $data = $this->teamService->getAllTeams();
        return $this->resultMessage( 'OK', 200, $data );

    }

    /**
     * @param Request $request
     * @param Team    $team
     * @return mixed
     */
    public function show( Request $request, Team $team )
    {
        $data = $this->teamService->getOneTeam( $team->id );
        return $this->resultMessage( 'OK', 200, $data );

    }
}
