<?php

namespace App\Http\Controllers;

use App\Services\CompetitionService;
use App\Traits\ApiResponser;

class CompetitionController extends Controller
{
    use ApiResponser;

    /**
     * @var CompetitionService
     */
    private $competitionService;

    public function __construct( CompetitionService $competitionService )
    {
        $this->competitionService = $competitionService;
    }

    public function index()
    {
        $response = $this->competitionService->getAllCompetition();

        if ( $response[ 'code' ] === 200 ) {
            return $this->resultMessage( 'OK', $response[ 'code' ], $response[ 'data' ] );
        } else {
            return $this->errorResponse( $response[ 'error' ], $response[ 'code' ] );
        }
    }

    public function show( $id )
    {
        $response = $this->competitionService->getOneCompetition( $id );

        if ( $response[ 'code' ] === 200 ) {
            return $this->resultMessage( 'OK', $response[ 'code' ], $response[ 'data' ] );
        } else {
            return $this->errorResponse( $response[ 'error' ], $response[ 'code' ] );
        }
    }
}
