<?php

namespace App\Exceptions;

use App\Traits\ApiResponser;
use Asm89\Stack\CorsService;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class Handler extends ExceptionHandler
{
    use ApiResponser;
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [//
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param Exception $exception
     * @return void
     */
    public function report( Exception $exception )
    {
        parent::report( $exception );
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param Request $request
     * @param Exception                $exception
     * @return Response
     */
    public function render( $request, Exception $exception )
    {
        $response = $this->handleException($request, $exception);

        app(CorsService::class)->addActualRequestHeaders($response, $request);

        return $response;
    }

    public function handleException( $request, Exception $exception )
    {

        if ( $exception instanceof ValidationException ) {
            return $this->convertValidationExceptionToResponse( $exception, $request );
        }

        if ( $exception instanceof ModelNotFoundException ) {
            $modelo = strtolower( class_basename( $exception->getModel() ) );
            return $this->errorResponse( "No existe ninguna instancia de {$modelo} con el id especificado", 404 );
        }
        if ( $exception instanceof AuthorizationException ) {
            return $this->errorResponse( 'No posee permisos para ejecutar esta acción', 403 );
        }

        if ( $exception instanceof NotFoundHttpException ) {
            return $this->errorResponse( 'No se encontró la URL especificada', 404 );
        }

        return $this->errorResponse( 'Falla inesperada. Intente luego', 500 );
    }
}
