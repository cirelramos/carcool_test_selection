<?php

namespace App\Traits;

use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response;

trait CallExternalApiTrait
{
    public function getData( $resource, $subResource = '', $attributes = '' )
    {
        try {
            $client   = $this->initClient();
            $url      = $this->urlBase() . "" . $resource;
            $response = $client->get( $url, $this->headers() );
            $data     = $this->collectData( $response, $resource, $subResource, $attributes );
            return $data;
        }
        catch ( Exception $e ) {
            $message = json_decode( $e->getMessage() ) !== null ? json_decode( $e->getMessage() ) : $e->getMessage();
            $code    = (int) ( $e->getCode() !== 0 ? $e->getCode() : 500 );
            $code    = $code < 100 ? 500 : $code;
            return [ 'error' => $message, 'code' => $code ];
        }
    }

    private function initClient()
    {
        $client = new Client();
        return $client;
    }

    private function urlBase()
    {
        return env( 'URL_EXTERNAL_API', '' );
    }

    private function headers()
    {
        $token = env( 'TOKE_EXTERNAL_API', '' );
        return [
            'headers' => [
                'X-Auth-Token' => $token,
            ],
        ];
    }

    private function collectData( Response $response, $resource = '', $subResource = '', $attributes = '' )
    {
        $data = $response->getBody();
        $data = json_decode( $data, true );
        $code = $response->getStatusCode();
        $data = $this->transformCollectData( $data, $resource, $subResource, $attributes );
        $data = [ 'data' => $data, 'code' => $code ];
        return $data;
    }

    private function transformCollectData( $data, $resource, $subResource, $attributes = '' )
    {
        if ( $attributes === '' ) {
            $activeResource          = $subResource === '' ? $resource : $subResource;
            $data = collect( $data[ $activeResource ] );
        }
        return (object) $data;
    }
}
