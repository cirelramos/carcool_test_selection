<?php

namespace App\Traits;

trait ApiResponser
{

    protected function errorResponse( $message, $code = 401 )
    {
        return response()->json( [ 'data' => [ 'error' => $message, 'code' => $code ] ], $code );
    }

    protected function resultMessage( $message, $code = 200, $result = null )
    {
        $response = [
            'data' => [
                'msj'  => $message,
                'code' => $code,
            ],
        ];
        if ( !empty( $result ) ) {
            $response[ 'data' ][ 'result' ] = $result;
        }
        return $this->successResponse( $response, $code );
    }

    protected function successResponse( $data, $code = 200 )
    {
        return response()->json( $data, $code );
    }
}
