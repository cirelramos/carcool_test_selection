<?php

namespace App\Providers;

use App\Repositories\CompetitionRepository;
use App\Repositories\CompetitionRepositoryInterface;
use App\Repositories\PlayerRepository;
use App\Repositories\PlayerRepositoryInterface;
use App\Repositories\TeamRepository;
use App\Repositories\TeamRepositoryInterface;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind( CompetitionRepositoryInterface::class, CompetitionRepository::class );
        $this->app->bind( TeamRepositoryInterface::class, TeamRepository::class );
        $this->app->bind( PlayerRepositoryInterface::class, PlayerRepository::class );
    }
}
