<?php

namespace App\Services;

use App\Repositories\CompetitionRepositoryInterface;
use App\Repositories\PlayerRepositoryInterface;
use App\Repositories\TeamRepositoryInterface;
use Exception;
use Illuminate\Support\Facades\DB;

class CompetitionService
{
    /**
     * @var CompetitionRepositoryInterface
     */
    private $competitionRepository;
    /**
     * @var TeamRepositoryInterface
     */
    private $teamRepository;
    /**
     * @var PlayerRepositoryInterface
     */
    private $playerRepository;

    /**
     * CompetitionService constructor.
     * @param CompetitionRepositoryInterface $competitionRepository
     * @param TeamRepositoryInterface        $teamRepository
     * @param PlayerRepositoryInterface      $playerRepository
     */
    public function __construct( CompetitionRepositoryInterface $competitionRepository,
        TeamRepositoryInterface $teamRepository,
        PlayerRepositoryInterface $playerRepository )
    {
        $this->competitionRepository = $competitionRepository;
        $this->teamRepository        = $teamRepository;
        $this->playerRepository      = $playerRepository;
    }

    /**
     * @return mixed
     */
    public function getAllCompetition()
    {
        return $this->competitionRepository->allCompetition();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getOneCompetition( $id )
    {
        $response      = $this->competitionRepository->oneCompetition( $id );
        $response_team = $this->competitionRepository->oneCompetitionWithTeams( $id );
        $this->saveTeam( $response_team );
        return $response;
    }

    public function saveTeam( $response )
    {
        if ( $response[ 'code' ] === 200 ) {
            $data      = $response[ 'data' ];
            $names     = $data->pluck( 'name' )->toArray();
            $existTeam = $this->teamRepository->getTeamByNameQuery( $names )->get( [ 'name' ] );
            $data      = $data->reject( $this->rejectTeamsDuplicate( $existTeam ) );
            $data      = $data->map( $this->mapTransformRemoveArea() );
            $dataArray = $data->toArray();

            DB::beginTransaction();
            try {
                $this->teamRepository->insert( $dataArray );
                DB::commit();
            }
            catch ( Exception $e ) {
                DB::rollback();
            }

            $data->map( $this->mapGetPlayersAndSave() );

        }
        return $response;
    }

    /**
     * @param $existTeam
     * @return callable
     */
    public function rejectTeamsDuplicate( $existTeam ): callable
    {
        return function ( $item ) use ( $existTeam ) {
            $exist = $existTeam->where( 'name', $item[ 'name' ] )->count();
            return ( $exist > 0 ? true : false );
        };
    }

    /**
     * @return callable
     */
    public function mapTransformRemoveArea(): callable
    {
        return function ( $item ) {
            unset( $item[ 'area' ] );
            return $item;
        };
    }

    /**
     * @return callable
     */
    public function mapGetPlayersAndSave(): callable
    {
        return function ( $item ) {
            $id_team = $item[ 'id' ];
            $players = $this->teamRepository->getPlayersByTeam( $id_team );
            if ( $players[ 'code' ] === 200 ) {
                $data        = $players[ 'data' ];
                $names       = $data->pluck( 'name' )->toArray();
                $existPlayer = $this->playerRepository->getPlayerByNameQuery( $names )->get( [ 'name' ] );
                $data        = $data->reject( $this->rejectPlayersDuplicate( $existPlayer ) );
                $data        = $data->map( $this->mapNewAttributes( $id_team ) );
                $dataArray   = $data->toArray();

                DB::beginTransaction();
                try {
                    $this->playerRepository->insert( $dataArray );
                    DB::commit();
                }
                catch ( Exception $e ) {
                    DB::rollback();
                }

            }
            return $item;
        };
    }

    /**
     * @param $existPlayer
     * @return callable
     */
    public function rejectPlayersDuplicate( $existPlayer ): callable
    {
        return function ( $item ) use ( $existPlayer ) {
            $exist = $existPlayer->where( 'name', $item[ 'name' ] )->count();
            return ( $exist > 0 ? true : false );
        };
    }

    /**
     * @param $id_team
     * @return callable
     */
    public function mapNewAttributes( $id_team ): callable
    {
        return function ( $item ) use ( $id_team ) {
            $newItem                  = [];
            $newItem[ 'name' ]        = $item[ 'name' ];
            $newItem[ 'position' ]    = $item[ 'position' ];
            $newItem[ 'shirtNumber' ] = $item[ 'shirtNumber' ];
            $newItem[ 'id_team' ]     = $id_team;
            return $newItem;
        };
    }
}
