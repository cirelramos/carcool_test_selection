<?php

namespace App\Services;

use App\Repositories\TeamRepositoryInterface;

class TeamService
{
    /**
     * @var TeamRepositoryInterface
     */
    private $teamRepository;

    /**
     * TeamService constructor.
     * @param TeamRepositoryInterface $teamRepository
     */
    public function __construct( TeamRepositoryInterface $teamRepository )
    {
        $this->teamRepository = $teamRepository;
    }

    public function getAllTeams()
    {
        return $this->teamRepository->getAllTeams();
    }

    public function getOneTeam( $id )
    {
        return $this->teamRepository->getOneTeam( $id );
    }
}
