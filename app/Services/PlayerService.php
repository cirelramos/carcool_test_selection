<?php

namespace App\Services;

use App\Repositories\PlayerRepositoryInterface;

class PlayerService
{
    /**
     * @var PlayerRepositoryInterface
     */
    private $playerRepository;

    /**
     * PlayerService constructor.
     * @param PlayerRepositoryInterface $playerRepository
     */
    public function __construct( PlayerRepositoryInterface $playerRepository )
    {
        $this->playerRepository = $playerRepository;
    }

    /**
     * @return mixed
     */
    public function getAllPlayers()
    {
        return $this->playerRepository->getAllPlayers();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getOnePlayer( $id )
    {
        return $this->playerRepository->getOnePlayer( $id );
    }

}
