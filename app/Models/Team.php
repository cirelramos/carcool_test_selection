<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @property varchar                                  $name        name
 * @property varchar                                  $shortName   shortName
 * @property varchar                                  $tla         tla
 * @property varchar                                  $crestUrl    crestUrl
 * @property varchar                                  $address     address
 * @property varchar                                  $phone       phone
 * @property varchar                                  $website     website
 * @property varchar                                  $email       email
 * @property varchar                                  $founded     founded
 * @property varchar                                  $clubColors  clubColors
 * @property varchar                                  $venue       venue
 * @property varchar                                  $lastUpdated lastUpdated
 * @property timestamp                                $created_at  created at
 * @property timestamp                                $updated_at  updated at
 * @property Collection $player      hasMany
 */
class Team extends Model
{

    /**
     * Database table name
     */
    protected $table = 'teams';

    /**
     * Mass assignable columns
     */
    protected $fillable = [
        'name',
        'shortName',
        'tla',
        'crestUrl',
        'address',
        'phone',
        'website',
        'email',
        'founded',
        'clubColors',
        'venue',
        'lastUpdated',
    ];

    /**
     * Date time columns.
     */
    protected $dates = [];

    /**
     * players
     *
     * @return HasMany
     */
    public function squad()
    {
        return $this->hasMany( Player::class, 'id_team' );
    }

}
