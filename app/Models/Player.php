<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @property varchar   $name        name
 * @property varchar   $position    position
 * @property varchar   $shirtNumber shirtNumber
 * @property int       $id_team     id team
 * @property timestamp $created_at  created at
 * @property timestamp $updated_at  updated at
 * @property IdTeam    $team        belongsTo
 */
class Player extends Model
{

    /**
     * Database table name
     */
    protected $table = 'players';

    /**
     * Mass assignable columns
     */
    protected $fillable = [
        'name',
        'position',
        'shirtNumber',
        'id_team',
    ];

    /**
     * Date time columns.
     */
    protected $dates = [];

    /**
     * idTeam
     *
     * @return BelongsTo
     */
    public function teams()
    {
        return $this->belongsTo( Team::class, 'id_team' );
    }

}
