## About Repo

API test selection carcool


## Instructions
* change ENV (TOKE_EXTERNAL_API, URL_EXTERNAL_API)
* create your database
* php artisan migrate:fresh --seed -n --force

## API consult List
* /api/competitions
* /api/competitions/{id}
* /api/teams
* /api/teams/{id}
* /api/players/
* /api/players/{id}


## explicacion de consulto de la api externa
* se uso la libreria  guzzle para hacer peticiones a la api externa 
* si utilizo try catch al momento de consultar a la api, para capturar los errores y retornar una respuesta adecuada
* al momento de obtener el team se realizan map y reject para organizar la data, validar sino esta duplicado, para luego realizar un insert masivo
* luego de  registrar el equipo se registran los jugadores, validando como el equipo
