<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware( 'auth:api' )->get( '/user', function ( Request $request ) {
    return $request->user();
} )
;
/*competitions*/
Route::get( 'competitions/', 'CompetitionController@index' )->name( 'competitions.index' );
Route::get( 'competitions/{id}', 'CompetitionController@show' )->name( 'competitions.show' );

/*teams*/
Route::get( 'teams/', 'TeamController@index' )->name( 'teams.index' );
Route::get( 'teams/{team}', 'TeamController@show' )->name( 'teams.show' );

/*players*/
Route::get( 'players/', 'PlayerController@index' )->name( 'players.index' );
Route::get( 'players/{player}', 'PlayerController@show' )->name( 'players.show' );
