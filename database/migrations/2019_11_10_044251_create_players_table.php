<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlayersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( 'players', function ( Blueprint $table ) {
            $table->increments( 'id' );
            $table->string( 'name', 160 );
            $table->string( 'position', 60 )->nullable();
            $table->string( 'shirtNumber', 160 )->nullable();
            $table->integer( 'id_team' )->unsigned()->comment( 'relation teams' );
            $table->foreign( 'id_team' )->references( 'id' )->on( 'teams' )->onDelete( 'cascade' );
            $table->timestamps();
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists( 'players' );
    }
}
