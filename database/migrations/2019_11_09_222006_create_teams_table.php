<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTeamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( 'teams', function ( Blueprint $table ) {
            $table->increments( 'id' );
            $table->string( 'name', 160 );
            $table->string( 'shortName', 60 )->nullable();
            $table->string( 'tla', 60 )->nullable();
            $table->string( 'crestUrl', 250 )->nullable();
            $table->string( 'address', 250 )->nullable();
            $table->string( 'phone', 190 )->nullable();
            $table->string( 'website', 190 )->nullable();
            $table->string( 'email', 190 )->nullable();
            $table->string( 'founded', 190 )->nullable();
            $table->string( 'clubColors', 190 )->nullable();
            $table->string( 'venue', 190 )->nullable();
            $table->string( 'lastUpdated', 190 )->nullable();
            $table->timestamps();
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists( 'teams' );
    }
}
